<?php 

  function escape($string) {
    return htmlentities($string, ENT_QUOTES, 'UTF-8');
  }
  
  function getSiteUrl($path=false){
    if($path){
      return Config::get('site_url').'/'.$path;
    }
    return Config::get('site_url');
  }

  
?>
