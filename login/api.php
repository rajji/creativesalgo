<?php
require('startup.php');
if( Input::exists('get') ){
  if( !empty(Input::get('path') )){
    $api = new Api();
    $path = Input::get('path');
    if(method_exists($api,$path)){
        echo json_encode($api->{$path}());
    }
  }
}
?>
