<?php
/**
 * Created by Chris on 9/29/2014 3:53 PM.
 */
require_once 'startup.php';
if (Input::exists()) {
    if(Token::check(Input::get('token'))) {
        $validate = new Validate();
        $validation = $validate->check($_POST, array(
            'name' => array(
                'name' => 'Name',
                'required' => true,
                'min' => 2,
                'max' => 50
            ),
            'username' => array(
                'name' => 'Username',
                'required' => true,
                'min' => 2,
                'max' => 20,
                'unique' => 'users'
            ),
            'password' => array(
                'name' => 'Password',
                'required' => true,
                'min' => 4
            ),
            'password_again' => array(
                'required' => true,
                'matches' => 'password'
            ),
        ));
        if ($validate->passed()) {
            $user = new User();
            $salt = Hash::salt(32);
            try {
                $user->create(array(
                    'name' => Input::get('name'),
                    'username' => Input::get('username'),
                    'password' => Hash::make(Input::get('password'), $salt),
                    'salt' => $salt,
                    'date_added' => date('Y-m-d H:i:s'),
                    'role_id' => 1
                ));
                Session::flash('home', 'Welcome ' . Input::get('username') . '! Your account has been registered. You may now log in.');
                Redirect::to('index.php');
            } catch(Exception $e) {
                echo $e, '<br>';
            }
        } else {
            foreach ($validate->errors() as $error) {
                echo $error . "<br>";
            }
        }
    }
}
?>

<!DOCTYPE html>
<html>
  <head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="<?php echo getSiteUrl('assets/css/materialize.min.css') ?>"  media="screen,projection"/>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  </head>

  <body>
    <main role="main" id="MainContent">
        <div class="section container">
          <div class="row">
            <div class="col s12">
              <h3>Register</h3>
            </div>
          </div>
          <div class="row">
              <form class="col s12"  action="" method="post">
                <div class="row">
                  <div class="input-field col s6">
                    
                    
                    <input id="name" type="text" class="validate"  name="name" value="<?php echo escape(Input::get('name')); ?>" />
                    
                    
                    <label for="name">Name</label>
                  </div>
                  <div class="input-field col s6">
                    <input type="text" class="validate" name="username" id="username" value="<?php echo escape(Input::get('username')); ?>">
                    <label for="username">Username</label>
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s12">
                    <input type="password" name="password" id="password">
                    <label for="password">Password</label>
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s12">
                    <input type="password" name="password_again" id="password_again" value="">
                    <label for="password_again">Repeat Password</label>
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s12">
                    <input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
                    <input type="submit" value="Register" class="waves-effect waves-light btn" />
                  </div>
                </div>
              </form>
            </div>
    </div>
    
    </main>

    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="<?php echo getSiteUrl('assets/js/materialize.min.js') ?>"></script>
  </body>
</html>
