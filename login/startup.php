<?php  
session_start();
loadLibrary('library');
loadLibrary('helper');
$file = explode('?',basename($_SERVER['REQUEST_URI']));
if( $file[0] != 'api.php' ){
  // var_dump( Session::exists(Config::get('remember.cookie_name')) );
  // exit;
  if(Cookie::exists( Config::get('remember.cookie_name') ) && !Session::exists(Config::get('sessions.session_name'))) {
      
      $hash = Cookie::get(Config::get('remember.cookie_name'));
      
      $hashCheck = DB::getInstance()->get('users_session', array('hash', '=', $hash));
      if($hashCheck->count()) {
          $user = new User($hashCheck->first()->user_id);
          $user->login();
      }
  }
}


function loadLibrary($dir){
  $ignore_path = array( 
    '.',
    '..'
  );
  $files = scandir($dir);
  foreach ( $files as $filename) {
    if( !in_array( $filename, $ignore_path ) ){
      $path = $dir . '/' . $filename;
      if (is_dir($path)) {
          loadLibrary($path);
      }
      elseif( is_file( $path ) ){
        require_once($path);
      }
    }
  }
}

?>
