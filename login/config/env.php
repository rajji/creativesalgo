<?php 
  return [
    'site_url'     => 'http://localhost/creativesalgo/login/',
    'mysql'        => [
        'host' => 'localhost',
        'username' => 'root',
        'password' => 'new',
        'db' => 'login'
    ],
    'remember'  => [
        'cookie_name' => 'hash',
        'cookie_expiry' => 604800
    ],
    'sessions'  => [
        'session_name' => 'user',
        'token_name' => 'token'
    ]
  ];
?>
