<?php 
  
/**
 * 
 */
class Api
{
  
  function login(){
    $data = [
      'error'       => true,
      'error_msg'   => 'No Input Specified',
      'data'        => null,
      'success'     => false
    ];
    if(Input::exists()) {
      if(Token::check(Input::get('token'),false)) {  
          $validate = new Validate();
          $validation = $validate->check($_POST, array(
              'username' => array(
                'required' => true,
                'min'      => 1,
              ),
              'password' => array(
                'required' => true,
                'min'      => 1,
              )
          ));
          if($validate->passed()) {
              $user = new User();
              $remember = (Input::get('remember') === 'on') ? true : false;
              $login = $user->login(Input::get('username'), Input::get('password'), $remember);
              if($login) {
                  $data['success'] = true;
                  $data['error'] = false;
                  $data['error_msg'] = null;
              } else {
                  $data['error_msg'] = 'Incorrect username or password.';
              }
          } else {
              $data['error_msg'] = implode(',',$validate->errors());
          }
      }
      return $data;
    }
  }
  
}


?>
