<?php 

/**
 * 
 */
 class DB {
     
     private static $_instance = null;
     
     private $_mysql;
     private $_query;
     private $_error = false;
     private $_results;
     private $_count = 0;
             
     private function __construct() {
         try {
             $this->_pdo = new PDO( 'mysql:host=' . Config::get('mysql.host') . ';dbname=' . Config::get('mysql.db'), Config::get('mysql.username'), Config::get('mysql.password'));
         } catch(PDOException $e) {
             die($e->getMessage());
         }
     }
     
     public function __destruct(){
       $this->_pdo = null;
     }
     
     public static function getInstance() {
         if(!isset(self::$_instance)) {
             self::$_instance = new DB();
         }
         return self::$_instance;
     }
     
     
     public function query($sql, $params = array()) {
         $this->_error = false;
         $this->_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
         try{
            $this->_query = $this->_pdo->prepare($sql);
            $x = 1;
            if(count($params)) {
                extract($params);  
                foreach($params as $key => $value) {
                   $this->_query->bindValue(':'.$key, ${$key});
                   $x++;
                }
                $this->_query->execute();
                $this->_results = $this->_query->fetchAll(PDO::FETCH_OBJ);
                $this->_count = $this->_query->rowCount();
             }
            return $this;
          }
         catch(PDOException $e){
           echo $sql . "<br>" . $e->getMessage();
           $this->_error = true;
         }
         
     }
     
     
     public function action($action, $table, $where = array()) {
         if(count($where) === 3) {
             $operators = array('=', '>', '<', '>=', '<=');
             $field = $where[0];
             $operator = $where[1];
             $value = $where[2];
             if(in_array($operator, $operators)) {
                 $sql = "{$action} FROM {$table} WHERE {$field} {$operator} :{$field}";
                 if($this->query($sql, array($field=>$value))) {
                     return $this;
                 }
             }
         }
         return false;
     }
     
     
     public function insert($table, $fields = array()) {
         $keys = array_keys($fields);
         $values = ':'.implode(', :',$keys);
         $sql = "INSERT INTO {$table} (`" . implode('`, `', $keys) . "`) VALUES ({$values})";
         if( $this->query($sql,$fields) === false){
           return $this->_pdo->errorInfo();
         }
         else{
           return true;
         }
     }
     
     
     public function update($table, $id, $fields) {
         $set = '';
         $x = 1;
         foreach($fields as $name => $value) {
             $set .= "{$name} = ?";
             if($x < count ($fields)) {
                 $set .= ', ';
             }
             $x++;
         }
         $sql = "UPDATE {$table} SET {$set} WHERE id = {$id}";
         if(!$this->query($sql, $fields)->error()) {
             return true;
         }
         return false;
     }
     
     
     public function delete($table, $where) {
         return $this->action('DELETE ', $table, $where);
     }
     
     public function get($table, $where, $fields = ['*']) {
         $fields = implode(', ',$fields);
         return $this->action("SELECT $fields", $table, $where);
     }
     
     public function results() {
         return $this->_results;
     }
     
     
     public function first() {
         $data = $this->results();
         return $data[0];
     }
     public function count() {
         return $this->_count;
     }
     public function error() {
         return $this->_error;
     }
 }


 ?>
