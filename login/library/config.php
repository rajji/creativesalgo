<?php 
/**
 * 
 */
class Config {
    
    private static $config;
    
    public static function get($path = null) {
        if(!isset(self::$config)){
          self::$config = require_once('config/env.php');
        }
        
        if ($path){
            $config = self::$config;
            $path = explode('.', $path);
            foreach($path as $bit) {
                if(isset($config[$bit])) {
                    $config = $config[$bit];
                }
            }
            return $config;
        }
        return false;
    }
    
}

?>
