<?php 
class Cookie {
    
    protected $_expiry_time;
    
    public static function exists($name) {
        return (isset($_COOKIE[$name])) ? true : false;
    }
    
    public static function get($name) {
        return $_COOKIE[$name];
    }
    
    public static function put($name, $value, $expiry = false) {
        if($expiry){
          $cookie = setcookie($name, $value, time() + $expiry, '/');
        }
        else{
          $cookie = setcookie($name, $value, 0, '/');
        }
        if($cookie) {
            return true;
        }
        return false;
    }
    
    public static function delete($name) {
        self::put($name, '', time() -1);
    }
    
}
 ?>  
