<?php 
/**
 * 
 */
class User {
  
  private $_db;
  private $_data;
  private $_sessionName;
  private $_cookieName;
  private $isLoggedIn;
  private $_role;
  private $_permissions = [];
  
  
  public function __construct($user = null) {
        $this->_db = DB::getInstance();
        $this->_sessionName = Config::get('sessions.session_name');
        $this->_cookieName = Config::get('remember.cookie_name');
        if(!$user) {
            if(Cookie::exists($this->_sessionName)) {
                $user = Cookie::get($this->_sessionName);
                if($this->find($user)) {
                    $this->isLoggedIn = true;
                } else {
                    $this->logout();
                }
            }
        } else {
           $this->find($user);
        }
    }
    
  
    public function create($fields = array()) {
        if(!$this->_db->insert('users', $fields)) {
          throw new Exception('Sorry, there was a problem creating your account;');
        }
    }
    
    
    public function update($fields = array(), $id = null) {
        if(!$id && $this->isLoggedIn()) {
            $id = $this->data()->user_id;
        }
        if(!$this->_db->update('users', $id, $fields)) {
            throw new Exception('There was a problem updating');
        }
    }
    
    
    public function find($user = null) {
        if($user) {
            $field = (is_numeric($user)) ? 'user_id' : 'username';
            $data = $this->_db->get('users', array($field, '=', $user));
            if($data->count()) {
                $this->_data = $data->first();
                return true;
            }
        }
        return false;
    }
    
    
    public function login($username = null, $password = null, $remember = false) {
        if(!$username && !$password && $this->exists()) {
            Cookie::put($this->_sessionName, $this->data()->user_id);
        } else {
            $user = $this->find($username);
            if ($user) {
                if ($this->data()->password === Hash::make($password, $this->data()->salt)) {
                    Cookie::put($this->_sessionName, $this->data()->user_id);
                    if ($remember) {
                        $hash = Hash::unique();
                        $hashCheck = $this->_db->get('users_session', array('user_id', '=', $this->data()->user_id));
                        if (!$hashCheck->count()) {
                            $this->_db->insert('users_session', array(
                                'user_id' => $this->data()->user_id,
                                'hash' => $hash
                            ));
                        } else {
                            $hash = $hashCheck->first()->hash;
                        }
                        Cookie::put($this->_cookieName, $hash, Config::get('remember.cookie_expiry'));
                    }
                    return $this->isLoggedIn = true;
                }
            }
        }
        return false;
    }
    
    
    public function hasPermission($key){
        if(empty($this->_role)){
            $role = $this->_db->get('user_roles', array('role_id', '=', $this->data()->role_id));
            if($role->count()) {
              $this->_role = $role->first()->role;
              
            }
        }
        return $this->_role == $key;
    }
    
    public function checkPermissions($permission){
        if(empty($this->_permissions)){
            $permissions = $this->_db->get('role_to_permission', array('role_id', '=', $this->data()->role_id),['permission']);
            if($permissions->count()){
              $this->_permissions = $permissions->results();
              $this->_permissions = array_column($this->_permissions,'permission');
            }
        }
        return in_array($permission,$this->_permissions);
    }
    
    public function exists() {
        return (!empty($this->_data)) ? true : false;
    }
    
    
    public function logout() {
      if(!empty($this->data())){
        $this->_db->delete('users_session', array('user_id', '=', $this->data()->user_id));
        Cookie::delete($this->_sessionName);
        Cookie::delete($this->_cookieName);
      }
      return true;
    }
    
    
    public function data(){
        return $this->_data;
    }
    
    
    public function isLoggedIn() {
        return $this->isLoggedIn;
    }
  
}


 ?>
