<?php 
  require_once('startup.php');  
  $user = new User(); //Current
  if($user->isLoggedIn()) {
    Redirect::to('index.php');
  }
?>
<!DOCTYPE html>
<html>
  <head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="<?php echo getSiteUrl('assets/css/materialize.min.css') ?>"  media="screen,projection"/>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  </head>

  <body>
    <main role="main" id="MainContent">
        <div class="section container">
            <div class="row">
              <div class="col s12 m6 offset-m3">
                <div class="card login-wrapper">
                  <div class="card-content">
                    <div id="CustomerLoginForm">
                      <form method="post" autocomplete="off" id="customer_login" accept-charset="UTF-8">
                        <input type="hidden" value="customer_login" name="form_type">
                        <input type="hidden" name="utf8" value="✓">
                        <h4 class="center">Login</h4>
                        <div class="input-field">
                          <label for="CustomerEmail" class="">
                            Email
                          </label>
                          <input type="text" autocomplete="off" name="username" id="username">
                        </div>
                          <div class="input-field">
                            <label for="CustomerPassword">
                              Password
                            </label>
                            <input type="password" autocomplete="off" name="password" id="password">
                          </div>
                          <div >
                            <p>
                              <input type="checkbox" id="remember" name="remember" value="on" />
                              <label for="remember">Remember</label>
                            </p>
                          </div>
                          <div class="input-field">
                            <input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
                            <input type="submit" class="btn-large z-depth-0" value="Log In">
                          </div>
                          <div class="input-field">
                            <a href="register.php">Create Account</a>
                          </div>
                            
                      </form>

                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
        <div class="form-success hide" id="ResetSuccess">
          We've sent you an email with a link to update your password.
        </div>
      </main>
    
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="<?php echo getSiteUrl('assets/js/materialize.min.js') ?>"></script>
    <script>
      document.getElementById('customer_login').addEventListener('submit',function(event){
        event.preventDefault();
        var formData = new FormData();
        formData.append('username',this.username.value);
        formData.append('password',this.password.value);
        formData.append('token',this.token.value);
        if(this.remember.checked){
            formData.append('remember',this.remember.value);
        }
        var url = '<?php echo getSiteUrl('api.php?path=login'); ?>';
        var xhttp = new XMLHttpRequest();
        xhttp.open("POST", url, true);
        xhttp.send(formData);
        xhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            var response = JSON.parse(this.responseText);
            if(response['success']){
              location.reload();
            }
            if(response['error']){
              alert(response['error_msg']);
            }
          }
        };
        
      });
    </script>
  </body>
</html>
        
